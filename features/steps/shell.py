from behave import *
import util
import os

@given('a timeout of {timeout} seconds is set')
def step_impl(context, timeout):
    context.timeout = int(timeout)

@when('command "{command}" is executed')
def step_impl(context, command):
    timeout = context.timeout if hasattr(context, 'timeout') else None
    result = util.run_shell_command(command, timeout=timeout)
    context.shell_result = result

@then('program succeeded')
def step_impl(context):
    assert(context.shell_result.completed_process.returncode == 0)

@then('snapshot succeeded')
def step_impl(context):
    assert(context.shell_result.snapshot_exit_value == 0)

@then('program failed')
def step_impl(context):
    assert(context.shell_result.completed_process.returncode != 0)

@then('snapshot failed')
def step_impl(context):
    assert(context.shell_result.snapshot_exit_value != 0)

@then('program had no stdout output')
def step_impl(context):
    assert(len(context.shell_result.completed_process.stdout) == 0)

@then('program had no stderr output')
def step_impl(context):
    assert(len(context.shell_result.completed_process.stderr) == 0)

@then('environment variables have the value')
def step_impl(context):
    for row in context.table:
        assert(context.shell_result.completed_env[row['env']] == row['value'])

@then('snapshot environment variables have the value')
def step_impl(context):
    for row in context.table:
        assert(context.shell_result.snapshot_env[row['env']] == row['value'])

@then('environment variables are set')
def step_impl(context):
    for row in context.table:
        assert(row['env'] in context.shell_result.completed_env.keys())

@then('snapshot environment variables are set')
def step_impl(context):
    for row in context.table:
        assert(row['env'] in context.shell_result.snapshot_env.keys())

@then('environment variables are unset')
def step_impl(context):
    for row in context.table:
        assert(row['env'] not in context.shell_result.completed_env.keys())

@then('snapshot environment variables are unset')
def step_impl(context):
    for row in context.table:
        assert(row['env'] not in context.shell_result.snapshot_env.keys())

@then('stdout contains "{stdout_text}"')
def step_impl(context, stdout_text):
    assert(stdout_text in context.shell_result.completed_process.stdout)

@then('stdout does not contain "{stdout_text}"')
def step_impl(context, stdout_text):
    assert(stdout_text not in context.shell_result.completed_process.stdout)

@then('stderr contains "{stderr_text}"')
def step_impl(context, stderr_text):
    assert(stderr_text in context.shell_result.completed_process.stderr)

@then('stderr does not contain "{stderr_text}"')
def step_impl(context, stderr_text):
    assert(stderr_text not in context.shell_result.completed_process.stderr)
