import subprocess
from subprocess import PIPE
import json

class CompletedShellCommand:
    def __init__(self, completed_process, completed_env, snapshot_exit_value, snapshot_env):
        self.completed_process = completed_process
        self.completed_env = completed_env
        self.snapshot_exit_value = snapshot_exit_value
        self.snapshot_env = snapshot_env

__ENV_START_TAG = "%%env_start%%"
__ENV_END_TAG = "%%env_end%%"

__SNAPSHOT_START_TAG = "%%snapshot_start%%"
__SNAPSHOT_END_TAG = "%%snapshot_end%%"

def run_shell_command(command, shell="/bin/bash", timeout=None):
    env_dict = None
    env_snapshot = None
    env_snapshot_exit_value = None
    command = command.replace("%%snapshot%%", f"""exit_code_snapshot_42=$?; echo "{__SNAPSHOT_START_TAG}"; echo $exit_code_snapshot_42; python3 -c 'import json; import os; print(json.dumps(dict(os.environ)))'; echo "{__SNAPSHOT_END_TAG}" """)
    completed_process = subprocess.run([shell, "-c", f"""{command}; exit_code_envtest_42=$?; python3 -c 'import json; import os; print("\\n{__ENV_START_TAG}\\n"+json.dumps(dict(os.environ))+"\\n{__ENV_END_TAG}")'; exit $exit_code_envtest_42"""], stdout=PIPE, stderr=PIPE, timeout=timeout)
    completed_process.stdout = completed_process.stdout.decode()
    completed_process.stderr = completed_process.stderr.decode()
    filtered_stdout = []
    env_json_lines = []
    env_snapshot_lines = []
    current_list = filtered_stdout
    for line in completed_process.stdout.splitlines():
        if line == __ENV_START_TAG:
            current_list = env_json_lines
            continue
        elif line == __SNAPSHOT_START_TAG:
            current_list = env_snapshot_lines
            continue
        elif line == __ENV_END_TAG or line == __SNAPSHOT_END_TAG:
            current_list = filtered_stdout
            continue
        current_list.append(line)
    if len(env_json_lines) > 0:
        env_dict = json.loads('\n'.join(env_json_lines))
        completed_process.stdout = '\n'.join(filtered_stdout)
    if len(env_snapshot_lines) > 0:
        env_snapshot_exit_value = int(env_snapshot_lines[0])
        env_snapshot = json.loads('\n'.join(env_snapshot_lines[1:]))
    return CompletedShellCommand(completed_process, env_dict, env_snapshot_exit_value, env_snapshot)
