Feature: doing stuff in shell

  Scenario: shell command with timeout
    Given a timeout of 10 seconds is set
     When command "export ZZZ=abc; ls -la; sleep 5; echo xxx; true" is executed
     Then program succeeded
      And program had no stderr output
      And stdout contains "xxx"
      And environment variables have the value
        | env   | value        |
        | HOME  | /home/vajdaz |
        | ZZZ   | abc          |

  Scenario: failing command
     When command ">&2 echo xxx; false" is executed
     Then program failed
      And stderr contains "xxx"

  Scenario: snapshot example
     When command "(export XXX=2; false; %%snapshot%%); true" is executed
     Then program succeeded
      And environment variables are unset
        | env |
        | XXX |
      And snapshot failed
      And snapshot environment variables are set
        | env |
        | XXX |
